module.exports = function(app, db) {


	app.get('/latest/:currency', function (request, response) {
		var currency = request.params.currency.toUpperCase();
		var currencies = [];

		// Validate user input format
		if (currency.match(/^[A-Z]{3}(-[A-Z]{3})?$/) === null){
			response.status(400).json({
				errorType: "Invalid currency format",
				description: "Please use a valid exchange rate format. For example .../latest/EUR or .../latest/EUR-USD"
			});
			return;
		}

		// Separate currencies from user input if required
		currencies = currency.split("-");

		if (currencies.length === 1){	// Single rate
			if (isValidCurrencyName(currency)){		// Validate Currency Name
				getSingleLatestCurrency(currency, response);
			} else {
				response.status(400).json({
					errorType: "Invalid currency",
					description: currency + " is an invalid currency."
				});
			}
		} else {	// Rate vs rate
			if (isValidCurrencyName(currencies[0])){ 	// Validate base currency Name
				if (isValidCurrencyName(currencies[1])){ 	// Validate currency Name
					getVsLatestCurrency(currencies[0], currencies[1], response);
				} else {
					response.status(400).json({
						errorType: "Invalid currency",
						description: currencies[1] + " is an invalid currency."
					});
				}
			} else {
				response.status(400).json({
					errorType: "Invalid currency",
					description: currencies[0] + " is an invalid currency."
				});
			}
		}
	});



	app.get('/latest/', function (request, response) {	// Send message error if currency name if missing
		response.status(400).json({
			errorType: "Missing currency names",
			description: "Please especify the currency name. For example .../latest/EUR or .../latest/EUR-USD"
		});
	});



	app.get('/historical/:currency', function (request, response) {
		var currency = request.params.currency.toUpperCase();

		// Validate user input format
		if (currency.match(/^[A-Z]{3}-[A-Z]{3}?$/) === null){
			response.status(400).json({
				errorType: "Invalid currency format",
				description: "Please use a valid exchange rate format. For example .../historical/EUR-USD"
			});
			return;
		}

		var currencies = currency.split("-");	// Divide currencies

		if (isValidCurrencyName(currencies[0])){ 	// Validate base currency Name
			if (isValidCurrencyName(currencies[1])){ 	// Validate currency Name
				if (request.query.date){	// Check if is historial single date

					// Validate date input format
					if (request.query.date.match(/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/) === null){
						response.status(400).json({
							errorType: "Invalid date format",
							description: "Please send date in this format mm/dd/yyyy"
						});
						return;
					}

					var today = new Date();
					var date = new Date(request.query.date);

					if (!(date instanceof Date && !isNaN(date))){	// Check if date valid date
						response.status(400).json({
							errorType: "Invalid date format",
							description: "Please send date in this format mm/dd/yyyy"
						});
					} else if (date > today){	// Check if is date is not greater than the current date.
						response.status(400).json({
							errorType: "Invalid date",
							description: "The date entered is greater than the current date."
						});
					} else {
						getHistoricalSingleDate(currencies[0], currencies[1], date, response);	// Get historical single date from DB
					}
				} else if (request.query.start){

					// Validate start date input format
					if (request.query.start.match(/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/) === null){
						response.status(400).json({
							errorType: "Invalid start date format",
							description: "Please send start date in this format mm/dd/yyyy"
						});
						return;
					}

					var today = new Date();
					var start = new Date(request.query.start);

					if (!(start instanceof Date && !isNaN(start))){	// Check if is a valid date
						response.status(400).json({
							errorType: "Invalid start date format",
							description: "Please send start date in this format mm/dd/yyyy"
						});
						return;
					}

					if (start > today){	// Check if start date is not greater than the current date.
						response.status(400).json({
							errorType: "Error Invalid date",
							description: "The start date is greater than the current date."
						});
						return;
					}

					if (request.query.end){

						// Validate end date input format
						if (request.query.end.match(/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/) === null){
							response.status(400).json({
								errorType: "Invalid end date format",
								description: "Please send end date in this format mm/dd/yyyy"
							});
							return;
						}

						var end = new Date(request.query.end);

						if (!(end instanceof Date && !isNaN(end))){	// Check if end date is a valid date
							response.status(400).json({
								errorType: "Invalid end date format",
								description: "Please send end date in this format mm/dd/yyyy"
							});
							return;
						}

						if (start >= end){	// Check if start date is greater than end date.
							response.status(400).json({
								errorType: "Error Invalid date",
								description: "Start date is greater or equal than end date."
							});
							return;
						}

						// Comentar
						getHistoricalRangeDate(currencies[0], currencies[1], start, end, response);

					} else {
						response.status(400).json({
							errorType: "Invalid end date value",
							description: "Please send end date value. For example: .../historical/GBP-USD?start=mm-dd-yyyy&end=mm-dd-yyyy"
						});
					}
				} else {
					response.status(400).json({
						errorType: "Invalid date value",
						description: "Please send the date or date ranges values. For example: .../historical/GBP-USD?date=mm-dd-yyyy or .../historical/GBP-USD?start=mm-dd-yyyy&end=mm-dd-yyyy"
					});
				}
			} else {
				response.status(400).json({
					errorType: "Invalid currency",
					description: currencies[1] + " is an invalid currency."
				});
			}
		} else {
			response.status(400).json({
				errorType: "Invalid currency",
				description: currencies[0] + " is an invalid currency."
			});
		}
	});



	app.get('/historical/', function (request, response) {
		response.status(400).json({
			errorType: "Missing currency names",
			description: "Please especify the currency name. For example .../historical/EUR-USD"
		});
	});



	app.get('*', function(request, response){	// Respond with 404 message if user try to get an invalid URL
		response.status(404).json({
			errorType: "Invalid URL",
			description: "Please check documentation and use a valid URL."
		});
	});



	function formatDate(date) {	// Format date to mm-dd-yyyy
		var retval = "";

		// Month
		if(date.getMonth() + 1 < 10){
		   retval += "0";
		 }
		retval += (date.getMonth() + 1);

		// Day
		retval += "-";
		if(date.getDate() < 10){
		   retval += "0";
		 }
		retval += date.getDate();

		// Year
		retval += "-" + date.getFullYear();

		return retval;
	}



	function isValidCurrencyName(currency){		// Validate if the currency is valid
		var validEx = {	// Valid currency names list
			EUR	: true,
			USD : true,
			JPY : true,
			BGN : true,
			CZK : true,
			DKK : true,
			GBP : true,
			HUF : true,
			PLN : true,
			RON : true,
			SEK : true,
			CHF : true,
			ISK : true,
			NOK : true,
			HRK : true,
			RUB : true,
			TRY : true,
			AUD : true,
			BRL : true,
			CAD : true,
			CNY : true,
			HKD : true,
			IDR : true,
			ILS : true,
			INR : true,
			KRW : true,
			MXN : true,
			MYR : true,
			NZD : true,
			PHP : true,
			SGD : true,
			THB : true,
			ZAR : true
		}

		return validEx[currency] === true;
	}



	function getSingleLatestCurrency(currency, response){	// Get lastes exchange rates from non vs request
		var res = {
			base: currency
		}

		var dbo = db.db("loyica");	// Open loyica db
		dbo.collection("exchange_rates").find().sort({date: -1}).limit(1).toArray(function(err, result) {	// Find newest exchange rates
			result = result[0];
			res.date = formatDate(result.date);	// Set date of newest exchange rates to response

			if (currency != "EUR"){	// Convert currencies rate values if necessary
				var rate = 1/result.rates[currency];	// Get EUR vs selected currency rate
				delete result.rates[currency];		// Remove selected currency from list

				for(var key in result.rates){
					result.rates[key] = Number((result.rates[key] * rate).toFixed(4));	// Convert currencies values based on rate
				}

				result.rates["EUR"] = Number(rate.toFixed(4));	// Add EUR to list
			} else {
				for (var i in result.rates){	// Convert strings to numbers
					result.rates[i] = Number(Number(result.rates[i]).toFixed(4));
				}
			}
			res.rates = result.rates;
			response.json(res);		// Send response to client
		});
	}



	function getVsLatestCurrency(currency1, currency2, response){	// Get lastes exchange rates from non VS request
		var res = {
			base: currency1,
			versus: currency2
		}

		var dbo = db.db("loyica");
		dbo.collection("exchange_rates").find().sort({date: -1}).limit(1).toArray(function(err, result) {	// Find newest exchange rates
			result = result[0];
			res.date = formatDate(result.date);	// Set date of newest exchange rates to response

			if (currency1 != "EUR"){	// Convert currencies values if necessary
				result.rates["EUR"] = 1;
				var rate = 1/result.rates[currency1];	// Get base vs selected currency rate
				res.rate = Number((result.rates[currency2] * rate).toFixed(4));	// Convert currencies values based on rate
			} else {
				res.rate = Number(result.rates[currency2]);
			}

			response.json(res);		// Send response to client
		});
	}



	function getHistoricalSingleDate(currency1, currency2, date, response){
		var res = {
			base: currency1,
			versus: currency2,
			date: formatDate(date)
		}

		var dbo = db.db("loyica");
		dbo.collection("exchange_rates").findOne({date: date}, function(err, result) {	// Find exchange rate

			// If there is no data on the required date
			if (result === null){
				response.json({
					title: "No Data",
					message: "There is no exchange data for " + res.date
				});
				return;
			}

			if (currency1 != "EUR"){	// Convert currencies values if necessary
				result.rates["EUR"] = 1;
				var rate = 1/result.rates[currency1];	// Get base vs selected currency rate
				res.rate = Number((result.rates[currency2] * rate).toFixed(4));	// Convert currencies values based on rate
			} else {
				res.rate = Number(result.rates[currency2]);
			}

			response.json(res);		// Send response to client
		});
	}



	function getHistoricalRangeDate(currency1, currency2, startDate, endDate, response){
		var res = {
			base: currency1,
			versus: currency2,
			start: formatDate(startDate),
			end: formatDate(endDate),
			rates: {}
		}

		var dbo = db.db("loyica");
		dbo.collection("exchange_rates").find({$and: [{date: {$gte: startDate}}, {date: {$lte: endDate}}]}).sort({date: 1}).toArray(function(err, result) {
			for (var i in result){
				var rateToSave;

				if (currency1 != "EUR"){	// Convert currencies values if necessary
					result[i].rates["EUR"] = 1;
					var rate = 1/result[i].rates[currency1];	// Get base vs selected currency rate
					rateToSave = Number((result[i].rates[currency2] * rate).toFixed(4));	// Convert currencies values based on rate
				} else {
					rateToSave = Number(result[i].rates[currency2]);
				}

				res.rates[formatDate(result[i].date)] = rateToSave;
			}

			response.json(res);
		});
	}


}
