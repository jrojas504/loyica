module.exports = function(app, db, schedule, dailySyncHour, http, ecbUrl, xmlParser) {


	schedule.scheduleJob('* 0 ' + dailySyncHour + ' * * *', function () { // Server daily update currencies. For more info please see: https://github.com/node-schedule/node-schedule
		updateExchangeRate();
	});


	function updateExchangeRate() {		// Get data from European Central Bank
		console.log("Updating exchange rates...");
		http.get(ecbUrl, (resp) => {	// Getting data from European Central Bank
			var completeResponse = '';

			resp.on('data', function (chunk) { completeResponse += chunk; });

			resp.on('end', function() {	// Read completed data
				parseExchangeRates(completeResponse);	// Parse data from XML to JSON
			});

		 }).on("error", (err) => {
			console.log("Error updating exchange rates...: " + err.message);
		 });

	}


	function parseExchangeRates(ex){	// Parse data from XML to JSON
		xmlParser(ex, function (err, result) {
			var exData = result["gesmes:Envelope"].Cube[0].Cube;

			// Mapping XML
			for(var i = 0; i < exData.length; i++ ){
				var dailyEx = {
					rates: {}
				};
				dailyEx.date = new Date(exData[i]["$"].time + "T06:00:00Z");	// Getting Date

				for(var j = 0; j < exData[i].Cube.length; j++){
					dailyEx.rates[exData[i].Cube[j]["$"].currency] = exData[i].Cube[j]["$"].rate; 	// Getting rates
				}

				saveExchangeRates(dailyEx); // Send data to save on db
			}
			console.log("Exchange rates updated!");
		});
	}


	function saveExchangeRates(ex){		// Save data on DB
		var dbo = db.db("loyica");	// Open loyica db
		dbo.collection("exchange_rates").findOne({date: ex.date}, function(err, result) { 	// Check if the element exist
			if (err) throw err;

			if (result === null){	// Save element if not exist on DB,
				dbo.collection("exchange_rates").insertOne(ex, function(err, res) {		// If not exist, add the new EX
					if (err) throw err;
				});
			}
		});
	}


	updateExchangeRate();
}
