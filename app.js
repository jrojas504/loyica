//		:::: Import Dependencies ::::
const express		= require('express');
const mongodb		= require('mongodb');
const http			= require('http');
const schedule		= require('node-schedule');
const xmlParser		= require('xml2js').parseString;
const app			= express();


//		:::: App Configuration ::::
const listeningPort	= 3000;
const dbUrl			= "mongodb://localhost:27017/";
const dailySyncHour	= 1;	// Server update daily at 1 am to get lastest exchange rates
const ecbUrl		= "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml";


//		:::: Serve Web App ::::
app.use('/', express.static('public'));



//		:::: DB Connection and start app ::::
mongodb.connect(dbUrl, {useNewUrlParser: true}, function(error, db) {
	if (error){
		console.error("Database connection error!");
	} else {

		//		:::: Run initial script to update exchange rate from European Central Bank ::::
		require('./app/initialScript.js')(app, db, schedule, dailySyncHour, http, ecbUrl, xmlParser);


		//		:::: Load services ::::
		require('./app/services.js')(app, db);


		//		:::: Server Listening ::::
		app.listen(listeningPort, () => console.log('Loyica exchange rate app listening on port ' + listeningPort + '!'));
	}
});

