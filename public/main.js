(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"loyica-app-wrapper\">\n\n\t<div class=\"logo-wrapper\">\n\t\t<img src=\"assets/logo-loyica.svg\" alt=\"Loyica Logo\">\n\t</div>\n\n\n\t<div class=\"exchange-form-wrapper\">\n\t\t<div class=\"base-currency-form\">\n\t\t\t<div class=\"dropdown base-dropdown\" tabindex=\"1\" (click)=\"openBaseCurrencyList($event)\" (focusout)=\"closeBaseCurrencyList()\" [ngClass]=\"{'active': activeClassOnBaseCurrency}\">\n\t\t\t\t<div class=\"select\">\n\t\t\t\t\t<img src=\"assets/flags/{{baseCurrency.code}}.png\">\n\t\t\t\t\t<span>{{baseCurrency.name + ' (' + baseCurrency.code + ')'}}</span>\n\t\t\t\t\t<i class=\"fa fa-chevron-left\"></i>\n\t\t\t\t</div>\n\t\t\t\t<input type=\"hidden\" name=\"base\">\n\t\t\t\t<ul class=\"dropdown-menu\" [ngClass]=\"{'display-list': activeClassOnBaseCurrency}\">\n\t\t\t\t\t<li class=\"dropdown-item\" *ngFor=\"let c of currencies\" (click)=\"selectBaseCurrency($event, c)\" >\n\t\t\t\t\t\t<img src=\"assets/flags/{{c.code}}.png\">\n\t\t\t\t\t\t<span>{{c.name}}</span>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\n\t\t\t<div class=\"input-wrapper\">\n\t\t\t\t<input [(ngModel)]=\"baseCurrencyValue\" (change)=\"convertCurrency()\" type=\"number\" name=\"base-currency\">\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t\t<div class=\"equal-sign\">=</div>\n\t\t\n\t\t<div class=\"vs-currency-form\">\n\t\t\t<div class=\"input-wrapper vs-input\">\n\t\t\t\t{{vsCurrencyValue}}\n\t\t\t</div>\n\t\t\t<div class=\"dropdown vs-dropdown\" tabindex=\"2\" (click)=\"openVsCurrencyList($event)\" (focusout)=\"closeVsCurrencyList()\" [ngClass]=\"{'active': activeClassOnVsCurrency}\">\n\t\t\t\t<div class=\"select\">\n\t\t\t\t\t<img src=\"assets/flags/{{vsCurrency.code}}.png\">\n\t\t\t\t\t<span>{{vsCurrency.name + ' (' + vsCurrency.code + ')'}}</span>\n\t\t\t\t\t<i class=\"fa fa-chevron-left\"></i>\n\t\t\t\t</div>\n\t\t\t\t<input type=\"hidden\" name=\"vsCurrency\">\n\t\t\t\t<ul class=\"dropdown-menu\" [ngClass]=\"{'display-list': activeClassOnVsCurrency}\">\n\t\t\t\t\t<li class=\"dropdown-item\" *ngFor=\"let c of currencies\" (click)=\"selectVsCurrency($event, c)\" >\n\t\t\t\t\t\t<img src=\"assets/flags/{{c.code}}.png\">\n\t\t\t\t\t\t<span>{{c.name}}</span>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t\n\t<div class=\"title\">Historical Currency Rates</div>\n\t\n\t\n\t<div class=\"historical-rate\">\n\t\t<div class=\"date-rate\">\n\t\t\tDisplay:  \n\t\t\t<select [(ngModel)]=\"historicalDate\" (change)=\"getHistoricalData()\">\n\t\t\t\t<option value=\"1\">1 Week</option>\n\t\t\t\t<option value=\"2\">1 Month</option>\n\t\t\t\t<option value=\"3\">3 Months</option>\n\t\t\t</select>\n\t\t</div>\n\t\t<canvas id=\"myChart\" style=\"position: relative; height:15vh; width:30vw\"></canvas>\n\t</div>\n\t\n\t\n\t<div class=\"title\">Other Currency Rates</div>\n\n\t\n\t<div class=\"other-currencies-table-wrapper\">\n\t\t<table class=\"other-currencies-table\">\n\t\t\t<tr>\n\t\t\t\t<th class=\"table-title\" colspan=\"2\">1 {{baseCurrency.code}} =</th>\n\t\t\t</tr>\n\t\t\t<tr class=\"table-body\" *ngFor=\"let item of baseRates\">\n\t\t\t\t<td class=\"currency-name\">{{item.code}}</td>\n\t\t\t\t<td class=\"currency-value\">{{item.rate}}</td>\n\t\t\t</tr>\n\t\t</table>\n\t</div>\n\t\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".loyica-app-wrapper .logo-wrapper {\n  text-align: center;\n  margin-top: 50px; }\n  .loyica-app-wrapper .logo-wrapper img {\n    width: 20%; }\n  @media only screen and (max-width: 1024px) {\n    .loyica-app-wrapper .logo-wrapper img {\n      width: 30%; } }\n  @media only screen and (max-width: 600px) {\n    .loyica-app-wrapper .logo-wrapper img {\n      width: 50%; } }\n  .loyica-app-wrapper .exchange-form-wrapper {\n  display: flex;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  margin: 0 auto;\n  margin-top: 50px; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown {\n    width: 200px;\n    display: inline-block;\n    background-color: #fff;\n    box-shadow: 0 0 2px #cccccc;\n    transition: all .5s ease;\n    position: relative;\n    font-size: 14px;\n    color: #474747;\n    height: 100%;\n    text-align: left;\n    outline: none;\n    float: left; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown .select {\n      cursor: pointer;\n      display: block;\n      padding: 10px;\n      white-space: nowrap;\n      overflow: hidden;\n      text-overflow: ellipsis; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown .select img {\n        height: 14px;\n        margin-right: 8px; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown .select > i {\n      font-size: 13px;\n      color: #888;\n      cursor: pointer;\n      transition: all .3s ease-in-out;\n      float: right;\n      line-height: 20px; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown .dropdown-menu {\n      transition: all .5s ease;\n      position: absolute;\n      background-color: #fff;\n      width: 100%;\n      height: 0;\n      left: 0;\n      margin-top: 1px;\n      box-shadow: 0 1px 2px #cccccc;\n      border-radius: 0 1px 5px 5px;\n      overflow: hidden;\n      max-height: 144px;\n      overflow-y: auto;\n      z-index: 9;\n      padding: 0;\n      list-style: none; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown .dropdown-menu .dropdown-item img {\n        height: 12px;\n        margin-right: 8px; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown .dropdown-menu li {\n      padding: 10px;\n      transition: all .2s ease-in-out;\n      cursor: pointer; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown .dropdown-menu li:hover {\n      background-color: #f2f2f2; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown .dropdown-menu li:active {\n      background-color: #e2e2e2; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown .display-list {\n      height: initial; }\n  .loyica-app-wrapper .exchange-form-wrapper .base-dropdown {\n    border-radius: 5px 0px 0px 5px; }\n  .loyica-app-wrapper .exchange-form-wrapper .vs-dropdown {\n    border-radius: 0px 5px 5px 0px; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown:hover {\n    box-shadow: 0 0 4px #cccccc; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown:active {\n    background-color: #f8f8f8; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown.active:hover,\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown.active {\n    box-shadow: 0 0 4px #cccccc;\n    border-radius: 5px 0px 0px 0px;\n    background-color: #f8f8f8; }\n  .loyica-app-wrapper .exchange-form-wrapper .vs-dropdown.active:hover,\n  .loyica-app-wrapper .exchange-form-wrapper .vs-dropdown.active {\n    box-shadow: 0 0 4px #cccccc;\n    border-radius: 0px 5px 0px 0px;\n    background-color: #f8f8f8; }\n  .loyica-app-wrapper .exchange-form-wrapper .dropdown.active .select > i {\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg); }\n  .loyica-app-wrapper .exchange-form-wrapper .input-wrapper {\n    float: left;\n    height: 37px;\n    width: 100px;\n    border-radius: 0px 5px 5px 0px;\n    background-color: #fff;\n    box-shadow: 0 0 2px #cccccc; }\n  .loyica-app-wrapper .exchange-form-wrapper .input-wrapper input {\n      outline: none;\n      width: 76px;\n      height: 16px;\n      font-size: 15px;\n      padding: 10px;\n      border: none; }\n  .loyica-app-wrapper .exchange-form-wrapper .vs-input {\n    border-radius: 5px 0px 0px 5px;\n    text-align: center;\n    line-height: 37px; }\n  .loyica-app-wrapper .exchange-form-wrapper .vs-input input {\n      margin-left: 4px; }\n  .loyica-app-wrapper .exchange-form-wrapper .equal-sign {\n    line-height: 37px;\n    font-size: 24px;\n    width: 60px;\n    text-align: center;\n    float: left; }\n  .loyica-app-wrapper .title {\n  text-align: center;\n  font-size: 20px;\n  margin-top: 80px;\n  margin-bottom: 20px;\n  color: gray; }\n  .loyica-app-wrapper .historical-rate {\n  text-align: center;\n  width: 50vw;\n  margin: 0 auto; }\n  .loyica-app-wrapper .historical-rate .date-rate {\n    margin-bottom: 10px; }\n  .loyica-app-wrapper .other-currencies-table-wrapper {\n  margin: 0 auto;\n  width: 15%; }\n  .loyica-app-wrapper .other-currencies-table-wrapper table, .loyica-app-wrapper .other-currencies-table-wrapper td, .loyica-app-wrapper .other-currencies-table-wrapper th {\n    border: 1px solid gray; }\n  .loyica-app-wrapper .other-currencies-table-wrapper .other-currencies-table {\n    width: 100%;\n    border-collapse: collapse; }\n  .loyica-app-wrapper .other-currencies-table-wrapper .other-currencies-table .table-title {\n      background-color: gray;\n      color: white;\n      font-weight: initial;\n      padding: 5px 10px;\n      text-align: center; }\n  .loyica-app-wrapper .other-currencies-table-wrapper .other-currencies-table .table-body {\n      background-color: white;\n      color: gray;\n      font-size: 14px; }\n  .loyica-app-wrapper .other-currencies-table-wrapper .other-currencies-table .table-body .currency-name {\n        padding: 5px;\n        text-align: center; }\n  .loyica-app-wrapper .other-currencies-table-wrapper .other-currencies-table .table-body .currency-value {\n        padding: 5px;\n        text-align: right; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(http) {
        this.http = http;
        this.activeClassOnBaseCurrency = false;
        this.activeClassOnVsCurrency = false;
        this.baseCurrency = {
            code: "EUR",
            name: "Euro"
        };
        this.vsCurrency = {
            code: "USD",
            name: "US Dollar"
        };
        this.baseCurrencyValue = 1;
        this.vsCurrencyValue = "";
        this.baseRates = [];
        this.historicalDate = "1";
        this.currencies = [{
                code: "USD",
                name: "US Dollar"
            }, {
                code: "JPY",
                name: "Japanese Yen"
            }, {
                code: "BGN",
                name: "Bulgarian Lev"
            }, {
                code: "CZK",
                name: "Czech Koruna"
            }, {
                code: "DKK",
                name: "Danish Krone"
            }, {
                code: "GBP",
                name: "Pound Sterling"
            }, {
                code: "HUF",
                name: "Hungarian Forint"
            }, {
                code: "PLN",
                name: "Polish Zloty"
            }, {
                code: "RON",
                name: "Romanian Leu"
            }, {
                code: "SEK",
                name: "Swedish Krona"
            }, {
                code: "CHF",
                name: "Swiss Franc"
            }, {
                code: "ISK",
                name: "Icelandic Krona"
            }, {
                code: "NOK",
                name: "Norwegian Krone"
            }, {
                code: "HRK",
                name: "Croatian Kuna"
            }, {
                code: "RUB",
                name: "Russian Ruble"
            }, {
                code: "TRY",
                name: "Turkish Lira"
            }, {
                code: "AUD",
                name: "Australian Dollar"
            }, {
                code: "BRL",
                name: "Brazilian Real"
            }, {
                code: "CAD",
                name: "Canadian Dollar"
            }, {
                code: "CNY",
                name: "Chinese Yuan Renminbi"
            }, {
                code: "HKD",
                name: "Hong Kong Dollar"
            }, {
                code: "IDR",
                name: "Indonesian Rupiah"
            }, {
                code: "ILS",
                name: "Israeli New Shekel"
            }, {
                code: "INR",
                name: "Indian Rupee"
            }, {
                code: "KRW",
                name: "South Korean Won"
            }, {
                code: "MXN",
                name: "Mexican Peso"
            }, {
                code: "MYR",
                name: "Malaysian Ringgit"
            }, {
                code: "NZD",
                name: "New Zealand Dollar"
            }, {
                code: "PHP",
                name: "Philippine Peso"
            }, {
                code: "SGD",
                name: "Singapore Dollar"
            }, {
                code: "THB",
                name: "Thai Baht"
            }, {
                code: "ZAR",
                name: "South African Rand"
            }, {
                code: "EUR",
                name: "Euro"
            }];
    }
    AppComponent.prototype.ngOnInit = function () {
        this.convertCurrency();
        this.getBaseCurrencyRates();
        this.getHistoricalData();
    };
    AppComponent.prototype.openBaseCurrencyList = function (event) {
        event.target.focus();
        this.activeClassOnBaseCurrency = true;
    };
    AppComponent.prototype.openVsCurrencyList = function (event) {
        event.target.focus();
        this.activeClassOnVsCurrency = true;
    };
    AppComponent.prototype.closeBaseCurrencyList = function () {
        this.activeClassOnBaseCurrency = false;
    };
    AppComponent.prototype.closeVsCurrencyList = function () {
        this.activeClassOnVsCurrency = false;
    };
    AppComponent.prototype.selectBaseCurrency = function (event, currency) {
        this.activeClassOnBaseCurrency = false;
        this.baseCurrency = currency;
        this.convertCurrency();
        this.getBaseCurrencyRates();
        this.getHistoricalData();
        event.stopPropagation();
    };
    AppComponent.prototype.selectVsCurrency = function (event, currency) {
        this.activeClassOnVsCurrency = false;
        this.vsCurrency = currency;
        this.convertCurrency();
        this.getHistoricalData();
        event.stopPropagation();
    };
    AppComponent.prototype.convertCurrency = function () {
        var _this = this;
        this.http.get('/latest/' + this.baseCurrency.code + '-' + this.vsCurrency.code).subscribe(function (data) {
            _this.vsCurrencyValue = (_this.baseCurrencyValue * data["rate"]).toFixed(2);
        });
    };
    AppComponent.prototype.getBaseCurrencyRates = function () {
        var _this = this;
        this.http.get('/latest/' + this.baseCurrency.code).subscribe(function (data) {
            var temp = [];
            for (var i in data["rates"]) {
                temp.push({
                    code: i,
                    rate: data["rates"][i]
                });
            }
            _this.baseRates = temp;
        });
    };
    AppComponent.prototype.getHistoricalData = function () {
        var today = new Date();
        var startDate = "";
        var endDate = this.formatDate(today);
        if (this.historicalDate == "1") {
            today.setDate(today.getDate() - 7);
            startDate = this.formatDate(today);
        }
        else if (this.historicalDate == "2") {
            today.setMonth(today.getMonth() - 1);
            startDate = this.formatDate(today);
        }
        else {
            today.setMonth(today.getMonth() - 3);
            startDate = this.formatDate(today);
        }
        this.http.get('/historical/' + this.baseCurrency.code + '-' + this.vsCurrency.code + '?start=' + startDate + '&end=' + endDate).subscribe(function (data) {
            var labels = [];
            var values = [];
            for (var i in data["rates"]) {
                labels.push(i);
                values.push(data["rates"][i]);
            }
            var ctx = document.getElementById("myChart");
			var myChart = new Chart(ctx, {
				type: 'line',
				data: {
					labels: labels,
					datasets: [{
						data: values,
						borderWidth: 2
					}]
				},
				options: {
					legend: {
						display: false
					},
					tooltips: {
						enabled: false
					}
				}
			});
        });
    };
    AppComponent.prototype.formatDate = function (date) {
        var retval = "";
        // Month
        if (date.getMonth() + 1 < 10) {
            retval += "0";
        }
        retval += (date.getMonth() + 1);
        // Day
        retval += "-";
        if (date.getDate() < 10) {
            retval += "0";
        }
        retval += date.getDate();
        // Year
        retval += "-" + date.getFullYear();
        return retval;
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/jrojas/Desktop/loyica-app/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map
